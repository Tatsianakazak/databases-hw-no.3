-- Write a query that will return for each year the most popular in rental film among films released in
-- one year.

WITH
    FilmRentalCounts
    AS
    (
        SELECT
            f.film_id,
            f.title,
            f.release_year,
            COUNT(r.rental_id) AS rental_count
        FROM
            film f
            JOIN
            inventory i ON f.film_id = i.film_id
            JOIN
            rental r ON i.inventory_id = r.inventory_id
        GROUP BY 
        f.film_id, f.title, f.release_year
    ),
    RankedFilms
    AS
    (
        SELECT
            film_id,
            title,
            release_year,
            rental_count,
            ROW_NUMBER() OVER (PARTITION BY release_year ORDER BY rental_count DESC) AS rank
        FROM
            FilmRentalCounts
    )
SELECT
    release_year,
    film_id,
    title,
    rental_count
FROM
    RankedFilms
WHERE 
    rank = 1
ORDER BY 
    release_year;

-- Write a query that will return the Top-5 actors who have appeared in Comedies more than anyone
-- else.

WITH
    ComedyFilms
    AS
    (
        SELECT
            f.film_id
        FROM
            film f
            JOIN
            film_category fc ON f.film_id = fc.film_id
            JOIN
            category c ON fc.category_id = c.category_id
        WHERE
        c.name = 'Comedy'
    ),
    ActorComedyAppearances
    AS
    (
        SELECT
            fa.actor_id,
            COUNT(*) AS appearance_count
        FROM
            film_actor fa
            JOIN
            ComedyFilms cf ON fa.film_id = cf.film_id
        GROUP BY
        fa.actor_id
    ),
    TopActors
    AS
    (
        SELECT
            ac.actor_id,
            ac.appearance_count,
            ROW_NUMBER() OVER (ORDER BY ac.appearance_count DESC) AS rank
        FROM
            ActorComedyAppearances ac
    )
SELECT
    a.actor_id,
    a.first_name,
    a.last_name,
    ta.appearance_count
FROM
    TopActors ta
    JOIN
    actor a ON ta.actor_id = a.actor_id
WHERE
    ta.rank <= 5
ORDER BY
    ta.rank;

-- Write a query that will return the names of actors who have not starred in “Action” films.

WITH
    ActionFilmActors
    AS
    (
        SELECT DISTINCT
            fa.actor_id
        FROM
            film_actor fa
            JOIN
            film_category fc ON fa.film_id = fc.film_id
            JOIN
            category c ON fc.category_id = c.category_id
        WHERE
        c.name = 'Action'
    )

SELECT
    a.actor_id,
    a.first_name,
    a.last_name
FROM
    actor a
    LEFT JOIN
    ActionFilmActors afa ON a.actor_id = afa.actor_id
WHERE
    afa.actor_id IS NULL;

-- Write a query that will return the three most popular in rental films by each genre.

WITH
    FilmRentalCounts
    AS
    (
        SELECT
            f.film_id,
            f.title,
            c.name AS genre,
            COUNT(r.rental_id) AS rental_count
        FROM
            film f
            JOIN
            film_category fc ON f.film_id = fc.film_id
            JOIN
            category c ON fc.category_id = c.category_id
            JOIN
            inventory i ON f.film_id = i.film_id
            JOIN
            rental r ON i.inventory_id = r.inventory_id
        GROUP BY
        f.film_id, f.title, c.name
    ),
    RankedFilms
    AS
    (
        SELECT
            film_id,
            title,
            genre,
            rental_count,
            ROW_NUMBER() OVER (PARTITION BY genre ORDER BY rental_count DESC) AS rank
        FROM
            FilmRentalCounts
    )
SELECT
    genre,
    film_id,
    title,
    rental_count
FROM
    RankedFilms
WHERE
    rank <= 3
ORDER BY
    genre,
    rank;

-- Calculate the number of films released each year and cumulative total by the number of films. Write
-- two query versions, one with window functions, the other without.

SELECT
    release_year,
    COUNT(*) AS films_released,
    SUM(COUNT(*)) OVER (ORDER BY release_year) AS cumulative_total
FROM
    film
GROUP BY
    release_year
ORDER BY
    release_year;

-- Calculate a monthly statistics based on “rental_date” field from “Rental” table that for each month
-- will show the percentage of “Animation” films from the total number of rentals. Write two query
-- versions, one with window functions, the other without.


SELECT
    rental_month,
    animation_rentals,
    total_rentals,
    animation_rentals * 100.0 / total_rentals AS animation_percentage
FROM (
    SELECT
        DATE_TRUNC('month', r.rental_date) AS rental_month,
        COUNT(*) AS total_rentals,
        SUM(CASE WHEN c.name = 'Animation' THEN 1 ELSE 0 END) AS animation_rentals
    FROM
        rental r
        JOIN
        inventory i ON r.inventory_id = i.inventory_id
        JOIN
        film_category fc ON i.film_id = fc.film_id
        JOIN
        category c ON fc.category_id = c.category_id
    GROUP BY
        DATE_TRUNC('month', r.rental_date)
) AS MonthlyStats
ORDER BY
    rental_month;

-- Write a query that will return the names of actors who have starred in “Action” films more than in
-- “Drama” film.

WITH
    ActorFilmCounts
    AS
    (
        SELECT
            a.actor_id,
            a.first_name,
            a.last_name,
            SUM(CASE WHEN c.name = 'Action' THEN 1 ELSE 0 END) AS action_count,
            SUM(CASE WHEN c.name = 'Drama' THEN 1 ELSE 0 END) AS drama_count
        FROM
            actor a
            JOIN
            film_actor fa ON a.actor_id = fa.actor_id
            JOIN
            film_category fc ON fa.film_id = fc.film_id
            JOIN
            category c ON fc.category_id = c.category_id
        GROUP BY
        a.actor_id, a.first_name, a.last_name
    )
SELECT
    actor_id,
    first_name,
    last_name,
    action_count,
    drama_count
FROM
    ActorFilmCounts
WHERE
    action_count > drama_count
ORDER BY
    first_name, last_name;

-- Write a query that will return the top-5 customers who spent the most money watching Comedies.

WITH
    ComedyPayments
    AS
    (
        SELECT
            p.customer_id,
            p.amount
        FROM
            payment p
            JOIN
            rental r ON p.rental_id = r.rental_id
            JOIN
            inventory i ON r.inventory_id = i.inventory_id
            JOIN
            film_category fc ON i.film_id = fc.film_id
            JOIN
            category c ON fc.category_id = c.category_id
        WHERE
        c.name = 'Comedy'
    ),
    CustomerSpending
    AS
    (
        SELECT
            cp.customer_id,
            SUM(cp.amount) AS total_spent
        FROM
            ComedyPayments cp
        GROUP BY
        cp.customer_id
    )
SELECT
    cs.customer_id,
    c.first_name,
    c.last_name,
    cs.total_spent
FROM
    CustomerSpending cs
    JOIN
    customer c ON cs.customer_id = c.customer_id
ORDER BY
    cs.total_spent DESC
LIMIT 5;

-- In the “Address” table, in the “address” field, the last word indicates the "type" of a street: Street,
-- Lane, Way, etc. Write a query that will return all "types" of streets and the number of addresses
-- related to this "type".

SELECT
    TRIM
(REGEXP_REPLACE
(address, '^.*\s(\S+)$', '\1')) AS street_type,
    COUNT
(*) AS address_count
FROM
    address
GROUP BY
    street_type
ORDER BY
    address_count DESC;

-- Write a query that will return a list of movie ratings, indicate for each rating the total number of
-- films with this rating, the top-3 categories by the number of films in this category and the number of
-- film in this category with this rating.

WITH
    RatingTotals
    AS
    (
        SELECT
            rating,
            COUNT(*) AS total_films
        FROM
            film
        GROUP BY 
        rating
    ),
    CategoryCounts
    AS
    (
        SELECT
            f.rating,
            c.name AS category,
            COUNT(*) AS film_count,
            ROW_NUMBER() OVER (PARTITION BY f.rating ORDER BY COUNT(*) DESC) AS category_rank
        FROM
            film f
            JOIN
            film_category fc ON f.film_id = fc.film_id
            JOIN
            category c ON fc.category_id = c.category_id
        GROUP BY 
        f.rating, c.name
    ),
    TopCategories
    AS
    (
        SELECT
            rating,
            category,
            film_count,
            category_rank
        FROM
            CategoryCounts
        WHERE 
        category_rank <= 3
    )
SELECT
    rt.rating,
    rt.total_films,
    STRING_AGG(tc.category || ': ' || tc.film_count, ', '
ORDER BY tc.category_rank
) AS top_categories
FROM 
    RatingTotals rt
JOIN 
    TopCategories tc ON rt.rating = tc.rating
GROUP BY 
    rt.rating, rt.total_films
ORDER BY 
    rt.total_films DESC;